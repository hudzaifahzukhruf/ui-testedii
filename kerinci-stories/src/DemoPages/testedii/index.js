import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText
} from 'reactstrap';

// USER PAGES

import Log from './Log';
import Images from './images';

const UserPages = ({ match }) => (
    <Fragment>
        <div className="app-container">
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">kerinci Stories</NavbarBrand>
                    <NavLink href="#/test/log">Log</NavLink>
                    <NavLink href="#/test/images">Images</NavLink>
                </Navbar>
            </div>

            {/* User Pages */}

            <Route path={`${match.url}/log`} component={Log} />
            <Route path={`${match.url}/images`} component={Images} />

        </div>
    </Fragment>
);

export default UserPages;