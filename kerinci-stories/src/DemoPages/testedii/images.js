import React, { Fragment } from 'react'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

import {
    Row, Col, Button, Form, FormGroup, Label, Input,
    Card, CardBody, CardHeader, Container, CardFooter,
} from 'reactstrap';

import ReactTable from "react-table";
import axios from 'axios';

export default class Images extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idRequestBooking: "",
            description: "",
            images: []
        };
        this.handleChangeImages = this.handleChangeImages.bind(this);
    }
    initialState = {
        idRequestBooking: "",
        description: ""
    }

    componentDidMount = async () => {
        await axios.get("http://localhost:8080/images/")
            .then(res => this.setState({ images: res.data }))

    }

    handleChangeImages = (event) => {

        const { name, value } = event.target;
        this.setState({ [name]: value });

    }

    submit = (event) => {
        event.preventDefault();
        var imagesInput = {
            idRequestBooking: this.state.idRequestBooking,
            description: this.state.description
        }
        axios.post("http://localhost:8080/images/", imagesInput).then(res => {
            if (res.status === 200) {
                this.componentDidMount();
                this.setState(this.initialState)
            } else {
                alert("error")
            }
        })
    }

    render() {
        const { idRequestBooking, description,images } = this.state;
        return (
            <Fragment>
                <CSSTransitionGroup
                    component="div"
                    transitionName="TabsAnimation"
                    transitionAppear={true}
                    transitionAppearTimeout={0}
                    transitionEnter={false}
                    transitionLeave={false}>
                    <br />
                    <br />
                    <br />
                    <br />
                    <Container fluid>
                        <h3 className="text-center"> Form Log </h3>
                        <br />
                        <Form onSubmit={this.submit}>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Label for="idRequestBooking" sm={2}>ID Request Booking</Label>
                                <Col sm={6}>
                                    <Input type="text" name="idRequestBooking" id="idRequestBooking"
                                        autoComplete="off" value={idRequestBooking} onChange={this.handleChangeImages}
                                        placeholder="Enter ID Request Booking"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Label for="description" sm={2}>Description</Label>
                                <Col sm={6}>
                                    <Input type="text" name="description" id="description"
                                        autoComplete="off" value={description} onChange={this.handleChangeImages}
                                        placeholder="Enter ID Platform"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Button type="submit"
                                    style={{ background: "#C0D39A", borderColor: "#C0D39A", color: "currentcolor" }}
                                >Submit</Button>
                            </FormGroup>
                        </Form>
                        <Row>
                            <Col md="12">
                                <Card className="main-card mb-3">
                                    <CardHeader>
                                        <h5>List Images</h5>
                                    </CardHeader>
                                    <CardBody>
                                        {
                                            this.state.images ?
                                                <>
                                                    <ReactTable
                                                        data={images}
                                                        columns={[{
                                                            columns: [
                                                                {
                                                                    Header: 'ID Request Booking',
                                                                    accessor: 'idRequestBooking',
                                                                    Cell: (row) => (
                                                                        <div className='d-block w-100 text-center'>
                                                                            {row.value}
                                                                        </div>)

                                                                },
                                                                {
                                                                    Header: 'Descriptiom',
                                                                    accessor: 'description',
                                                                    Cell: (row) => (
                                                                        <div className='d-block w-100 text-center'>
                                                                            {row.value}
                                                                        </div>)
                                                                }
                                                            ]
                                                        },]}
                                                        defaultPageSize={5}
                                                        className="-striped -highlight"
                                                    />
                                                </>
                                                : ""
                                        }
                                    </CardBody>
                                    <br />
                                    <CardFooter>
                                        <h1> </h1>
                                    </CardFooter>
                                </Card>
                            </Col>
                        </Row>
                    </Container>               
                </CSSTransitionGroup>
            </Fragment>
        )
    }
}