import React, { Fragment } from 'react'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

import {
    Row, Col, Button, Form, FormGroup, Label, Input,
    Card, CardBody, CardHeader, Container, CardFooter,
} from 'reactstrap';

import ReactTable from "react-table";
import axios from 'axios';

export default class Log extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idRequestBooking: "",
            idPlatform: "",
            namaPlatform: "",
            docType: "",
            termOfPayment: "",
            log: [],
            images: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);

    }
    initialStateLog = {
        idRequestBooking: "",
        idPlatform: "",
        namaPlatform: "",
        docType: "",
        termOfPayment: "",
    }

    componentDidMount = async () => {
        await axios.get("http://localhost:8080/log/")
            .then(res => this.setState({ log: res.data }))

    }

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value });

    }

    submit = (event) => {
        event.preventDefault();
        var logInput = {
            idRequestBooking: this.state.idRequestBooking,
            idPlatform: this.state.idPlatform,
            namaPlatform: this.state.namaPlatform,
            docType: this.state.docType,
            termOfPayment: this.state.termOfPayment
        }
        axios.post("http://localhost:8080/log/", logInput).then(res => {
            if (res.status === 200) {
                this.componentDidMount();
                this.setState(this.initialState)
            } else {
                alert("error")
            }
        })
    }

    
    render() {
        const { idPlatform, namaPlatform, docType, termOfPayment, log,
            images, idRequestBooking1, idRequestBooking2, description } = this.state;
        return (
            <Fragment>
                <CSSTransitionGroup
                    component="div"
                    transitionName="TabsAnimation"
                    transitionAppear={true}
                    transitionAppearTimeout={0}
                    transitionEnter={false}
                    transitionLeave={false}>
                    <br />
                    <br />
                    <br />
                    <br />

                    <Container fluid>
                        <h3 className="text-center"> Form Log </h3>
                        <br />
                        <Form onSubmit={this.submit}>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Label for="idRequestBooking" sm={2}>ID Request Booking</Label>
                                <Col sm={6}>
                                    <Input type="text" name="idRequestBooking" id="idRequestBooking"
                                        autoComplete="off" value={idRequestBooking1} onChange={this.handleChange}
                                        placeholder="Enter ID Request Booking"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Label for="idPlatform" sm={2}>ID Platform</Label>
                                <Col sm={6}>
                                    <Input type="text" name="idPlatform" id="idPlatform"
                                        autoComplete="off" value={idPlatform} onChange={this.handleChange}
                                        placeholder="Enter ID Platform"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Label for="namaPlatform" sm={2}>Nama Platform</Label>
                                <Col sm={6}>
                                    <Input type="text" name="namaPlatform" id="namaPlatform"
                                        autoComplete="off" value={namaPlatform} onChange={this.handleChange}
                                        placeholder="Enter Nama Platform"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Label for="docType" sm={2}>Doc Type</Label>
                                <Col sm={6}>
                                    <Input type="text" name="docType" id="docType"
                                        autoComplete="off" value={docType} onChange={this.handleChange}
                                        placeholder="Enter Doc Type"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Label for="termOfPayment" sm={2}>Term Of Payment</Label>
                                <Col sm={6}>
                                    <Input type="text" name="termOfPayment" id="termOfPayment"
                                        autoComplete="off" value={termOfPayment} onChange={this.handleChange}
                                        placeholder="Enter Term Of Payment"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row style={{ justifyContent: "center" }}>
                                <Button type="submit"
                                    style={{ background: "#C0D39A", borderColor: "#C0D39A", color: "currentcolor" }}
                                >Submit</Button>
                            </FormGroup>
                        </Form>
                        <Row>
                            <Col md="12">
                                <Card className="main-card mb-3">
                                    <CardHeader>
                                        <h5>List Log</h5>
                                    </CardHeader>
                                    <CardBody>
                                        {
                                            this.state.log ?
                                                <>
                                                    <ReactTable
                                                        data={log}
                                                        columns={[{
                                                            columns: [
                                                                {
                                                                    Header: 'ID Request Booking',
                                                                    accessor: 'idRequestBooking',
                                                                    Cell: (row) => (
                                                                        <div className='d-block w-100 text-center'>
                                                                            {row.value}
                                                                        </div>)

                                                                },
                                                                {
                                                                    Header: 'ID Platform',
                                                                    accessor: 'idPlatform',
                                                                    Cell: (row) => (
                                                                        <div className='d-block w-100 text-center'>
                                                                            {row.value}
                                                                        </div>)
                                                                },
                                                                {
                                                                    Header: 'Nama Platform',
                                                                    accessor: 'namaPlatform',
                                                                    Cell: row => (
                                                                        <div className='d-block w-100 text-center'>
                                                                            {row.value}
                                                                        </div>)
                                                                },
                                                                {
                                                                    Header: 'Doc Type',
                                                                    accessor: 'docType',
                                                                    Cell: row => (
                                                                        <div className='d-block w-100 text-center'>
                                                                            {row.value}
                                                                        </div>)
                                                                },
                                                                {
                                                                    Header: 'Term Of Payment',
                                                                    accessor: 'termOfPayment',
                                                                    Cell: row => (
                                                                        <div className='d-block w-100 text-center'>
                                                                            {row.value}
                                                                        </div>)
                                                                }
                                                            ]
                                                        },]}
                                                        defaultPageSize={5}
                                                        className="-striped -highlight"
                                                    />
                                                </>
                                                : ""
                                        }
                                    </CardBody>
                                    <br />
                                    <CardFooter>
                                        <h1> </h1>
                                    </CardFooter>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </CSSTransitionGroup>
            </Fragment>
        )
    }
}