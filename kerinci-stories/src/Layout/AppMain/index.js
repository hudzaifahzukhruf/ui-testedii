import { Route, Redirect} from 'react-router-dom';
import React, {Suspense, lazy, Fragment} from 'react';
import Loader from 'react-loaders'

import {
    ToastContainer,
} from 'react-toastify';
const Test = lazy(()=> import('../../DemoPages/testedii'))
const AppMain = () => {

    return (
        <Fragment>
            <Suspense 
            fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="semi-circle-spin"/>
                        </div>
                        <h6 className="mt-3">
                            Your booking form will show up in a minute
                            <small>Please wait...</small>
                        </h6>
                    </div>
                </div>
            }
            >
                <Route path="/test" component={Test}/>
            </Suspense>



            <Route exact path="/" render={() => (
                <Redirect to="/test/log"/>
            )}/>
            <ToastContainer/>
        </Fragment>
    )
};

export default AppMain;